Feature: Automating the DemoWebShop Application

  Scenario Outline: Login Functionality of DemoWebShop 
    Given open the website"https://demowebshop.tricentis.com/"
    When Click the Login link
    And Enter the email "<Email>" in the Email field
    And Enter the password "<Password>" in Password field
    And Click the login button
    
    Examples:
	|Email								|Password	|
	|manzmehadi1@gmail.com|Mehek@110|

  Scenario: Automating the Books Module of DemoWebShop
   
    Given Click the Books icon from the categories
    And Sort the books from price High to low
    When select two books 
    And Add the products to cart
    
  Scenario: Automating the Electronics Module of DemoWebShop
    Given select Electronics icon from the categories
    And Select cellphones from Electronics
    When Add  product into Cart
    Then display the Count of items added to the cart.
    
   Scenario: Automating the Gift cards Module of DemoWebShop
		Given select Gift cards icon from the categories
		And Select "4" in the display 
		Then Capture the name and price of one of the Gift cards displayed
		
		Scenario: LogOut Functionality of DemoWebShop
			Given Click the LogOut link
			And Check whether the Login icon is displayed or not
    
    
    
